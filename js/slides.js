(function ($) {
	var $slides = $('#b-slides');
	if ($slides.length) {
		var $html = $('html'),
			$navbar = $('.b-navbar');

		$slides.fullpage({
			scrollBar: false,
			resize: false,
			anchors: ['main', 'teachers', 'pupils', 'features'],
			autoScrolling: false,
			paddingTop: '60px',
			scrollingSpeed: 300,
			navigation: true,
			navigationPosition: 'right',
			fitToSection: false,
			verticalCentered: true,
			scrollOverflow: false,
			sectionSelector: '.b-slide',
			onLeave: function (index, nextIndex, direction) {
				if (nextIndex > 1) {
					$html.addClass('navbar-always-visible');
					$navbar.removeClass('b-navbar__status-inactive');
				} else {
					$html.removeClass('navbar-always-visible');
					$navbar.addClass('b-navbar__status-inactive');
				}

				if ([2, 4].indexOf(nextIndex) > -1) {
					$html.addClass('white-slide');
				} else {
					$html.removeClass('white-slide');
				}
			}
		});
	}
})(jQuery);