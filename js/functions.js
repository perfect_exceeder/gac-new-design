/**
 * Created by h8every1 on 07.02.2015.
 */
(function ($) {
	var $html = $('html'),
		body = $('body'),
		_window = $(window);

	var $navbar = $('.b-navbar'),
		$dropdowns = $('.b-dropdown'),
		$dropdown_bg = $('.b-dropdown-bg'),
		$active_dropdown, $active_submenu, $generated_cols,
		$submenus = $('.b-dropdown--submenu'),
		$first_lvl_links = $('.b-dropdown--link__depth-1'),
		$courses = $('.b-course');

	// screen sizes from bootstrap
	var screen_size = {
		'xs': 480, 'sm': 768, 'md': 992, 'lg': 1200
	};
	var css_sizes = ['size-phone', 'size-tablet', 'size-desktop'];

	// for slides on main page
	function init() {
		_window.trigger('resize.gac');
	}

	// top menu hover transition
	$navbar
		.hoverIntent({
			over: function () {
				if ($html.hasClass('b-slides-page')) {
					$navbar.removeClass('b-navbar__status-inactive');
				}
			},
			out: function () {
				if ($html.hasClass('b-slides-page') && !$html.hasClass('navbar-always-visible')) {
					$navbar.addClass('b-navbar__status-inactive');
				}
				dropdowns_hide();
			}
		});

	// only hide navbar on main page
	if ($html.hasClass('b-slides-page')) {
		$navbar
			.addClass('b-navbar__status-inactive');
	} else {
		$navbar
			.removeClass('b-navbar__status-inactive');
	}

	// submenu click handlers
	body.on('click.topmenu.gac', '.b-navbar--link', function (e) {
		var self = $(this), _dropdown = $(self.attr('href'));
		submenus_hide();

		if (_dropdown.length && $html.hasClass(css_sizes[2])) {
			e.preventDefault();
			if (!dropdown_is_visible(_dropdown)) {
				dropdown_show(_dropdown);
			}
		} else {
			dropdowns_hide();
		}
	}).on('click.topmenu.gac', '.b-dropdown--link__depth-1', function (e) {
		e.preventDefault();
	}).on('focus.gac', '.b-form-control', function () {
		// hide dropdowns when focusing on a form in navbar
		dropdowns_hide();
	}).hoverIntent({
		selector: '.b-dropdown--link__depth-1',
		over: function () {
			var _submenu = $(this).next('.b-dropdown--submenu');
			if (_submenu.length && !_submenu.is(':visible')) {
				$first_lvl_links.removeClass('active');
				$(this).addClass('active');
				submenu_show(_submenu);
			} else {
				$first_lvl_links.removeClass('active');
				//submenus_hide();
			}
		},
		out: function () {
		}

	});

	// set classes for different screen sizes and recalculate submenu height

	_window.on('resize.gac', function () {
		var win_width = realWinWidth(), win_height = realWinHeight();

		$html.removeClass(css_sizes.join(' '));

		if (win_width >= screen_size.xs && win_width < screen_size.sm) {
			$html.addClass(css_sizes[0]);
		} else if (win_width >= screen_size.sm && win_width < screen_size.md) {
			$html.addClass(css_sizes[1]);
		} else {
			$html.addClass(css_sizes[2]);
		}

		dropdown_bg_set_height();
		dropdown_bg_set_padding();
	});

	var dropdowns_hide = function () {
		if ($active_dropdown) {
			$dropdowns.removeClass('b-dropdown__visible');
			$dropdown_bg.hide();
			$active_dropdown = false;
		}
	};

	var dropdown_show = function (dropdown) {
		dropdowns_hide();
		$active_dropdown = dropdown;
		$active_dropdown.addClass('b-dropdown__visible');

		dropdown_bg_set_height();
		dropdown_bg_set_padding();

		// find first item with submenu
		var $first_submenu = $active_dropdown.find('.b-dropdown--submenu').filter(":first"),
			$link = $first_submenu.parent().find('a.b-dropdown--link__depth-1').filter(":first");
		$link.addClass('active');

		submenu_show($first_submenu);
	};

	var dropdown_is_visible = function (dropdown) {
		return dropdown.hasClass('b-dropdown-visible');
	};

	var dropdown_bg_set_height = function (height) {
		if ($active_dropdown) {
			if (typeof height == "undefined") {
				height = $active_dropdown.outerHeight();
			}

			$dropdown_bg.height(height);

			$dropdown_bg.show();
		}
	};

	var dropdown_bg_set_padding = function () {
		if ($active_dropdown) {
			var padding = $active_dropdown.offset().left + $active_dropdown.outerWidth(true)
			$dropdown_bg.css({'padding-left': padding});
			$dropdown_bg.show();
		}
	};

	var submenus_hide = function () {
		if ($active_submenu) {
			$submenus.hide();
			$active_submenu = false;
			if ($generated_cols) {
				$generated_cols.remove();
			}
		}
		dropdown_bg_set_height();
	};

	var submenu_show = function (submenu) {

		if (!submenu && $active_submenu) {
			submenu = $active_submenu;
		}

		submenus_hide();
		$active_submenu = submenu;

		var cols = $active_submenu.data('columns') || 3;

		var children = submenu.children(), items_per_column = Math.ceil(children.length / cols);
		var columns = [];

		for (var i = 0; i < cols; i++) {
			var items_in_col = items_per_column;
			while ($(children[items_in_col - 1]).hasClass('b-dropdown--item__header')) {
				items_in_col--;
			}

			columns.push(children.splice(0, items_in_col));
		}

		var _html = '';
		$.each(columns, function (index, value) {
			_html += '<ul class="b-submenu--column b-submenu--column__cols-' + cols + '">';
			$.each(value, function (idx, v) {
				_html += v.outerHTML;
			});
			_html += '</ul>';

		});

		var _submenu_left = $active_dropdown.outerWidth(true),
			_submenu_width = $('.container').offset().left + $('.container').outerWidth() - ($active_dropdown.offset().left + _submenu_left);

		$generated_cols = $('<div class="b-dropdown--submenu-container">' + _html + '</div>')
			.css({left: _submenu_left, width: _submenu_width})
			.insertAfter(submenu).show();


		dropdown_bg_set_height($generated_cols.outerHeight(true));
	};

	function realWinWidth() {
		return _window.width() > window.innerWidth ? _window.width() : window.innerWidth;
	}

	function realWinHeight() {
		return _window.height() > window.innerHeight ? _window.height() : window.innerHeight;
	}

	// init css classes on <html> element for main page slides
	init();


	// show courses table on course page
	$courses.on('click.gac', '.b-subtitle__course a', function (e) {
		e.preventDefault();
		$(this).parents('.b-course').toggleClass('b-course__open');
	});

	// toggle video transcript
	body.on('click.gac','.b-video--transcript-link',function(e){
		e.preventDefault();
		var _toggler = $(this).parents('.b-video--transcript-toggle'), _transcript = _toggler.next('.b-transcript');
		_toggler.toggleClass('b-video--transcript-toggle__on');
		_transcript.toggleClass('active').jScrollPane({
			showArrows: true,
			verticalGutter: 30});
	});

	var carousel_inited, $carousel_container = $('.b-video-carousel--container');
	var video_carousel = function() {
		if ($.isFunction($.fn.jCarouselLite) && !carousel_inited && $carousel_container.is(':visible')) {
			$carousel_container.jCarouselLite({
				btnNext: '.b-video-carousel--control__next',
				btnPrev: '.b-video-carousel--control__prev',
				responsive: true,
				autoWidth: true,
				circular: false,
				visible: 4,
				mouseWheel: false
			});
			carousel_inited = true;
		}
	};
	video_carousel();
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		//e.target // newly activated tab
		//e.relatedTarget // previous active tab
		video_carousel();
	});


	// feedback button
	body.on('click.gac','.b-feedback--tumbler',function(e){
		var _feedback = $(this).parent(),
			elements =[$('#vk_group'), _feedback.find('.b-feedback--social')];
		e.preventDefault();
		_feedback.toggleClass('active');

		if (_feedback.hasClass('active')) {
			$.each(elements, function (key, element) {
				element.show();
			});
			var i = 0;
			while (i < elements.length && _feedback.outerHeight() > realWinHeight()) {
				elements[i].hide();
				i++;
			}
		}
	});

})(jQuery);